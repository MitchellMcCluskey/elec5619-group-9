// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.smartliving.domain;

import com.smartliving.domain.SmartUser;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect SmartUser_Roo_Configurable {
    
    declare @type: SmartUser: @Configurable;
    
}
