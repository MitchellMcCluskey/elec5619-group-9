package com.smartliving.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findSmartUsersByUsernameEquals" })
public class SmartUser {

    /**
     */
    private String username;

    /**
     */
    private String password;

    /**
     */
    @NotNull
    private Boolean enabled;

    /**
     */
    @ManyToMany(cascade = CascadeType.PERSIST)
    private Set<SmartUserRole> roles = new HashSet<SmartUserRole>();
}
