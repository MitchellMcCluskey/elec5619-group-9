// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.smartliving.domain;

import com.smartliving.domain.SmartUser;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect SmartUser_Roo_Finder {
    
    public static TypedQuery<SmartUser> SmartUser.findSmartUsersByUsernameEquals(String username) {
        if (username == null || username.length() == 0) throw new IllegalArgumentException("The username argument is required");
        EntityManager em = SmartUser.entityManager();
        TypedQuery<SmartUser> q = em.createQuery("SELECT o FROM SmartUser AS o WHERE o.username = :username", SmartUser.class);
        q.setParameter("username", username);
        return q;
    }
    
}
