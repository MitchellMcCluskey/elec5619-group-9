package com.smartliving.domain;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import javax.validation.constraints.Min;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.ManyToOne;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(finders = { "findSmokingGoalsBySmartUser" })
public class SmokingGoal {

    /**
     */
    @Min(0L)
    private double currentCigarettesPerDay;
    
    /**
     */
    @Min(0L)
    private double cigarettesGoal;

    /**
     */
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date goalDate;

    /**
     */
    @ManyToOne
    private SmartUser smartUser;
}
