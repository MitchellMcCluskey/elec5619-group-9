// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.smartliving.domain;

import com.smartliving.domain.SmokingData;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

privileged aspect SmokingData_Roo_Jpa_Entity {
    
    declare @type: SmokingData: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long SmokingData.id;
    
    @Version
    @Column(name = "version")
    private Integer SmokingData.version;
    
    public Long SmokingData.getId() {
        return this.id;
    }
    
    public void SmokingData.setId(Long id) {
        this.id = id;
    }
    
    public Integer SmokingData.getVersion() {
        return this.version;
    }
    
    public void SmokingData.setVersion(Integer version) {
        this.version = version;
    }
    
}
