// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.smartliving.web;

import com.smartliving.domain.SmartUser;
import com.smartliving.domain.SmokingData;
import com.smartliving.domain.SmokingGoal;
import com.smartliving.web.ApplicationConversionServiceFactoryBean;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;

privileged aspect ApplicationConversionServiceFactoryBean_Roo_ConversionService {
    
    declare @type: ApplicationConversionServiceFactoryBean: @Configurable;
    
    public Converter<SmartUser, String> ApplicationConversionServiceFactoryBean.getSmartUserToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.smartliving.domain.SmartUser, java.lang.String>() {
            public String convert(SmartUser smartUser) {
                return new StringBuilder().append(smartUser.getUsername()).append(' ').append(smartUser.getPassword()).toString();
            }
        };
    }
    
    public Converter<Long, SmartUser> ApplicationConversionServiceFactoryBean.getIdToSmartUserConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.smartliving.domain.SmartUser>() {
            public com.smartliving.domain.SmartUser convert(java.lang.Long id) {
                return SmartUser.findSmartUser(id);
            }
        };
    }
    
    public Converter<String, SmartUser> ApplicationConversionServiceFactoryBean.getStringToSmartUserConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.smartliving.domain.SmartUser>() {
            public com.smartliving.domain.SmartUser convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), SmartUser.class);
            }
        };
    }
    
    public Converter<SmokingData, String> ApplicationConversionServiceFactoryBean.getSmokingDataToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.smartliving.domain.SmokingData, java.lang.String>() {
            public String convert(SmokingData smokingData) {
                return new StringBuilder().append(smokingData.getCigarettesSmoked()).append(' ').append(smokingData.getCreationDate()).toString();
            }
        };
    }
    
    public Converter<Long, SmokingData> ApplicationConversionServiceFactoryBean.getIdToSmokingDataConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.smartliving.domain.SmokingData>() {
            public com.smartliving.domain.SmokingData convert(java.lang.Long id) {
                return SmokingData.findSmokingData(id);
            }
        };
    }
    
    public Converter<String, SmokingData> ApplicationConversionServiceFactoryBean.getStringToSmokingDataConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.smartliving.domain.SmokingData>() {
            public com.smartliving.domain.SmokingData convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), SmokingData.class);
            }
        };
    }
    
    public Converter<SmokingGoal, String> ApplicationConversionServiceFactoryBean.getSmokingGoalToStringConverter() {
        return new org.springframework.core.convert.converter.Converter<com.smartliving.domain.SmokingGoal, java.lang.String>() {
            public String convert(SmokingGoal smokingGoal) {
                return new StringBuilder().append(smokingGoal.getCurrentCigarettesPerDay()).append(' ').append(smokingGoal.getCigarettesGoal()).append(' ').append(smokingGoal.getGoalDate()).toString();
            }
        };
    }
    
    public Converter<Long, SmokingGoal> ApplicationConversionServiceFactoryBean.getIdToSmokingGoalConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.Long, com.smartliving.domain.SmokingGoal>() {
            public com.smartliving.domain.SmokingGoal convert(java.lang.Long id) {
                return SmokingGoal.findSmokingGoal(id);
            }
        };
    }
    
    public Converter<String, SmokingGoal> ApplicationConversionServiceFactoryBean.getStringToSmokingGoalConverter() {
        return new org.springframework.core.convert.converter.Converter<java.lang.String, com.smartliving.domain.SmokingGoal>() {
            public com.smartliving.domain.SmokingGoal convert(String id) {
                return getObject().convert(getObject().convert(id, Long.class), SmokingGoal.class);
            }
        };
    }
    
    public void ApplicationConversionServiceFactoryBean.installLabelConverters(FormatterRegistry registry) {
        registry.addConverter(getSmartUserToStringConverter());
        registry.addConverter(getIdToSmartUserConverter());
        registry.addConverter(getStringToSmartUserConverter());
        registry.addConverter(getSmokingDataToStringConverter());
        registry.addConverter(getIdToSmokingDataConverter());
        registry.addConverter(getStringToSmokingDataConverter());
        registry.addConverter(getSmokingGoalToStringConverter());
        registry.addConverter(getIdToSmokingGoalConverter());
        registry.addConverter(getStringToSmokingGoalConverter());
    }
    
    public void ApplicationConversionServiceFactoryBean.afterPropertiesSet() {
        super.afterPropertiesSet();
        installLabelConverters(getObject());
    }
    
}
