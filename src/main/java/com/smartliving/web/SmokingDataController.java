package com.smartliving.web;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.smartliving.domain.SmartUser;
import com.smartliving.domain.SmokingData;
import com.smartliving.domain.SmokingGoal;
import com.googlecode.charts4j.AxisLabels;
import com.googlecode.charts4j.AxisLabelsFactory;
import com.googlecode.charts4j.Data;
import com.googlecode.charts4j.DataUtil;
import com.googlecode.charts4j.GCharts;
import com.googlecode.charts4j.LineChart;
import com.googlecode.charts4j.Plot;
import com.googlecode.charts4j.Plots;
import com.googlecode.charts4j.Shape;

import static com.googlecode.charts4j.Color.*;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/smokingdatas")
@Controller
@Transactional
@RooWebScaffold(path = "smokingdatas", formBackingObject = SmokingData.class)
public class SmokingDataController {

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid SmokingData smokingData, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, smokingData);
            return "smokingdatas/create";
        }
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
		List<SmartUser> smartUserList = SmartUser.findSmartUsersByUsernameEquals(username).getResultList();
		SmartUser user = smartUserList.get(0);
        EntityManager em = SmokingGoal.entityManager();
        
        Format dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String inputDate = dateFormatter.format(smokingData.getCreationDate());
        
        String updateStringQuery = "UPDATE smartliving.smoking_data	SET cigarettes_smoked= " + Double.toString(smokingData.getCigarettesSmoked()) + " WHERE creation_date= '" + inputDate + " 12:00:00' AND smart_user= " + user.getId();
        Query updateQuery = em.createNativeQuery(updateStringQuery);
        updateQuery.executeUpdate();
        
        uiModel.asMap().clear();
        smokingData.persist();
        smokingData.remove();
        return "redirect:/smokingdatas/";
    }
    
	@RequestMapping(produces = "text/html")
	public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		List<SmartUser> smartUserList = SmartUser.findSmartUsersByUsernameEquals(username).getResultList();
		if(!smartUserList.isEmpty()) {
			SmartUser user = smartUserList.get(0);
			Format dateFormatter = new SimpleDateFormat("dd");
			List<String> dateList = new ArrayList<String>();
			
			List<SmokingGoal> resultGoalList = SmokingGoal.findSmokingGoalsBySmartUser(user).getResultList();
			if (!resultGoalList.isEmpty()){
				List<Double> goalList = new ArrayList<Double>();
				
				for (SmokingGoal goalDataItem : resultGoalList) {
					goalList.add(goalDataItem.getCurrentCigarettesPerDay());
					dateList.add(dateFormatter.format(goalDataItem.getGoalDate()));
				}
				Data squeezedGoalData = DataUtil.scaleWithinRange(0, 50, goalList);
				Plot goalPlot = Plots.newPlot(squeezedGoalData);

				List<SmokingData> resultSmokingList = SmokingData.findSmokingDatasBySmartUser(user).getResultList();
				List<Double> smokingList = new ArrayList<Double>();
				
				double startSmoke = goalList.get(0);
				double savedTotal = 0;
				for (SmokingData smokingDataItem : resultSmokingList) {
					double cigarettesSmoked = smokingDataItem.getCigarettesSmoked();
					if (cigarettesSmoked != -1){
						savedTotal += startSmoke - cigarettesSmoked;
					}
					smokingList.add(cigarettesSmoked);
				}
				Data squeezedSmokingData = DataUtil.scaleWithinRange(0, 50, smokingList);
				Plot smokingPlot = Plots.newPlot(squeezedSmokingData);
				
				double cigaretteIndividualCost = 25/18;
				double cigaretteTotalCost = savedTotal * cigaretteIndividualCost;
				double minutesPerCigarette = 7;
				double minutesSaved = savedTotal * minutesPerCigarette;
				double hoursSaved = minutesSaved / 60;
				
				List<Plot> plots = new ArrayList<Plot>();
				smokingPlot.setColor(TOMATO);
				smokingPlot.addShapeMarkers(Shape.CIRCLE, TOMATO, 5);
				smokingPlot.setLegend(username + " Smoking Data");
				goalPlot.setColor(POWDERBLUE);
				goalPlot.setLegend(username + " Goal Cigarettes");
				plots.add(smokingPlot);
				plots.add(goalPlot);
				LineChart chart = GCharts.newLineChart(plots);
				AxisLabels xAxis = AxisLabelsFactory.newAxisLabels(dateList);
				chart.addXAxisLabels(xAxis);
				chart.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(0, 50));
				chart.setTitle(username + " Smoking Progress");
				chart.setSize(900, 330);
				uiModel.addAttribute("chart", chart.toURLString());
				String savingMessage = "Based on the data you have provided, you have saved a total of $" + String.format("%1$,.2f", cigaretteTotalCost) + ". The health benefits of this include saving of " + String.format("%1$,.2f", minutesSaved) + " minutes or " + String.format("%1$,.2f", hoursSaved) + " hours of your life.";
				uiModel.addAttribute("savingmessage", savingMessage);
				
				Format dateFormatterLong = new SimpleDateFormat("dd-MM-yyyy");
				String feedMessage= "I have saved a total of $" + String.format("%1$,.2f", cigaretteTotalCost) + " and increased my life expectancy by " + String.format("%1$,.2f", minutesSaved) + " minutes or " + String.format("%1$,.2f", hoursSaved) + " hours by reducing the amount I have smoked since " + dateFormatterLong.format(resultGoalList.get(0).getGoalDate()) + ". Join me at SmartLiving if you need help quitting smoking.";
				uiModel.addAttribute("feedmessage", feedMessage);
				
				String userTitle = username + "'s Quit Smoking Progress";
				uiModel.addAttribute("usertitle", userTitle);
				String introMessage = "Below is a graph of your progress. The blue line is the suggested target of cigarettes per day required to meet your goal. The red line is your actual smoking data. If the red line is below or on the blue line then you are meeting or exceeding your current progress goal.";
				uiModel.addAttribute("intromessage", introMessage);
			}
			else{
				String introMessage = "You have not set a goal yet. Please set a goal before reviewing your progress.";
				uiModel.addAttribute("intromessage", introMessage);
				
				String savingMessage = "";
				uiModel.addAttribute("savingmessage", savingMessage);
			}
		}
		addDateTimeFormatPatterns(uiModel);
		return "smokingdatas/list";
	}
}
