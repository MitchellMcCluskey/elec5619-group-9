package com.smartliving.web;

import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.roo.addon.web.mvc.controller.converter.RooConversionService;

import com.smartliving.domain.SmartUserRole;

/**
 * A central place to register application converters and formatters. 
 */
@RooConversionService
public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {

	@SuppressWarnings("deprecation")
	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
		registry.addConverter(getSmartUserRoleToStringConverter());
	}
	
	public Converter<SmartUserRole, String> getSmartUserRoleToStringConverter() {
		return new org.springframework.core.convert.converter.Converter<SmartUserRole, String>() {
			public String convert(SmartUserRole role) {
				return new StringBuilder().append(role.getRoleName()).toString();
			}
		};
	}
}
