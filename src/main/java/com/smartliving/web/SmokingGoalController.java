package com.smartliving.web;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.smartliving.domain.SmartUser;
import com.smartliving.domain.SmokingGoal;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/smokinggoals")
@Controller
@Transactional
@RooWebScaffold(path = "smokinggoals", formBackingObject = SmokingGoal.class)
public class SmokingGoalController {
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid SmokingGoal smokingGoal, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, smokingGoal);
            return "smokinggoals/create";
        }
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
		List<SmartUser> smartUserList = SmartUser.findSmartUsersByUsernameEquals(username).getResultList();
		SmartUser user = smartUserList.get(0);
        EntityManager em = SmokingGoal.entityManager();
        
        String deletionStringQuery = "DELETE FROM smartliving.smoking_goal WHERE smart_user = " + user.getId();
        Query deletionQuery = em.createNativeQuery(deletionStringQuery);
        deletionQuery.executeUpdate();
        
        String dataDeletionStringQuery = "DELETE FROM smartliving.smoking_data WHERE smart_user = " + user.getId();
        Query dataDeletionQuery = em.createNativeQuery(dataDeletionStringQuery);
        dataDeletionQuery.executeUpdate();
        
        Format dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        
        String endDateString = dateFormatter.format(smokingGoal.getGoalDate());
        LocalDate endDate = new LocalDate(endDateString);
        LocalDate setDate = new LocalDate();
        
        int endDay = endDate.getDayOfYear();
        int startDay = setDate.getDayOfYear();
        int duration = endDay - startDay;
        while (duration < 0){
        	duration += 365;
        }
        
        double cutAmount  = smokingGoal.getCurrentCigarettesPerDay() - smokingGoal.getCigarettesGoal();
        double goalAmount = smokingGoal.getCigarettesGoal();
        double setAmount = smokingGoal.getCurrentCigarettesPerDay();
        double reduceAmount = cutAmount/duration;
        
        LocalTime defaultTime = new LocalTime(12, 0);
        
        DateTimeFormatter fmt = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss");
        
        while (setDate.isBefore(endDate) || setDate.equals(endDate)){
        	
        	String date = fmt.print(setDate.toDateTime(defaultTime));
        	String goalStringQuery = "INSERT INTO smartliving.smoking_goal VALUES (NULL, " + goalAmount + ", " + setAmount + ", '" + date + "', NULL, " + user.getId() +")";
        	Query goalQuery = em.createNativeQuery(goalStringQuery, SmokingGoal.class);
        	goalQuery.executeUpdate();
        	String dataStringQuery = "INSERT INTO smartliving.smoking_data VALUES (NULL, -1, '" + date + "', NULL, " + user.getId() +")";
        	Query dataQuery = em.createNativeQuery(dataStringQuery, SmokingGoal.class);
        	dataQuery.executeUpdate();
        	setDate = setDate.plusDays(1);
        	setAmount -= reduceAmount;
        }
        
        uiModel.asMap().clear();
        smokingGoal.persist();
        smokingGoal.remove();
        return "redirect:/smokingdatas/";
    }
}
