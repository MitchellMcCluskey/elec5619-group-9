package com.smartliving.web;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.smartliving.domain.SmartUser;
import com.smartliving.domain.SmartUserRole;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/smartusers")
@Controller
@RooWebScaffold(path = "smartusers", formBackingObject = SmartUser.class)
public class SmartUserController {
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid SmartUser smartUser, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
    	if (bindingResult.hasErrors()) {
    		populateEditForm(uiModel, smartUser);
    	}

    	try {
    		String hashedPassword = sha256(smartUser.getPassword());
    		smartUser.setPassword(hashedPassword);
    		uiModel.asMap().clear();
    		List<SmartUserRole> roleList = SmartUserRole.findSmartUserRolesByRoleNameEquals("USER").getResultList();
    		Set<SmartUserRole> roleSet = new HashSet<SmartUserRole>(roleList);
    		if (!userHasAuthority("ADMINISTRATOR")){
    			smartUser.setRoles(roleSet);
    		}
    			
    		smartUser.setEnabled(true);
    		smartUser.persist();
    		return "redirect:/";
    	} catch (NoSuchAlgorithmException e) {
    		e.printStackTrace();
    	} catch (UnsupportedEncodingException e) {
    		e.printStackTrace();
    	}

    	return "logusers/create";
    }
    
	public static boolean userHasAuthority(String authority)
	{
		@SuppressWarnings("unchecked")
		List<GrantedAuthority> authorities = (List<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();

		for (GrantedAuthority grantedAuthority : authorities) {
			if (authority.equals(grantedAuthority.getAuthority())) {
				return true;
			}
		}

		return false;
	}

    private String sha256(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    	MessageDigest digest = MessageDigest.getInstance("SHA-256");
    	digest.update(password.getBytes("UTF-8"));
    	byte[] hash = digest.digest();
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0; i < hash.length; i++) {
    		sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
    	}
    	return sb.toString();
    }
}
