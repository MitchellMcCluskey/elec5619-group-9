// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.smartliving.domain;

import com.smartliving.domain.SmartUserRoleDataOnDemand;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect SmartUserRoleDataOnDemand_Roo_Configurable {
    
    declare @type: SmartUserRoleDataOnDemand: @Configurable;
    
}
